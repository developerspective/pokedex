import { Injectable } from '@angular/core';

import { SwUpdate } from '@angular/service-worker';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root'
})
export class UpdateService {
    constructor(private swUpdate: SwUpdate, private snackbar: MatSnackBar) {
        if (!swUpdate.isEnabled) {
            console.log('Service Worker is not enabled');
        }
        this.checkforUpdate();
    }

    public checkforUpdate(): void {
        this.swUpdate.available.subscribe(() => this.promptUser());
    }

    private promptUser(): void {
        const snackbar = this.snackbar.open('An update is Available', 'Reload', {
            duration: 10 * 1000
        });
        snackbar
            .onAction()
            .subscribe(() => {
                this.swUpdate.activateUpdate()
                    .then(() => window.location.reload());
            });
    }
}
